const pgPool = require('pg-pool');
const moment = require('moment');
const config = require('./config.json');
const pool = new pgPool(config);

exports.handler = async(event) => {

    var name = event.pathParameters.name;
    var dateOfBirth = JSON.parse(event.body).dateOfBirth;

    var dateCheck = moment(dateOfBirth, 'YYYY-MM-DD', true).isValid();
    
    if(dateCheck == false) {
        const response = {
            statusCode: 406,
            body: JSON.stringify("The date is not valid!!!")
        };
        return response;
    }
    
    return checkUser(name, dateOfBirth).then(result => {
        if(result.count < 1) {
                return saveUser(name, dateOfBirth).then(result => {
                    const response = {
                        statusCode: 204,
                        body: JSON.stringify()
                    };
                    return response;
                }).catch(error => {
                    console.error(error.toString());
                    const response = {
                        statusCode: 500,
                        body: JSON.stringify("Something went wrong. Error no: 1001")
                    };
                    return response;
                })
        } else {
                const response = {
                  statusCode: 400,
                  body: JSON.stringify("This user exists.")
                };
                
                return response;
        }
    }).catch(error => {
        console.error(error.toString());
        const response = {
          statusCode: 500,
          body: JSON.stringify("Something went wrong. Error no: 1002")
        };
        
        return response;
    })
    
};

function checkUser(name, birthday) {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT COUNT(1) AS COUNT FROM users WHERE name = $1 AND birthday = $2`,
                [name, birthday]
            )
            .then(result => {
                resolve(result.rows[0]);
            }).catch(error => {
                reject(error);
            });
        }
    );
}


function saveUser(name, birthday) {
    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO users (name, birthday) 
                VALUES ($1, $2) returning id`,
                [name, birthday]
            )
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
        }
    );
}