provider "aws" {
  region     = "eu-central-1"
  access_key = ""
  secret_key = ""
}

data "archive_file" "put_lambda_zip" {
    type          = "zip"
    source_dir    = "./put"
    output_path   = "put_endpoint.zip"
}

resource "aws_lambda_function" "put_endpoint" {
  filename         = "put_endpoint.zip"
  function_name    = "put_endpoint"
  role             = "arn:aws:iam::1111111:role/service-role/myrole1"
  handler          = "index.handler"
  source_code_hash = "${data.archive_file.put_lambda_zip.output_base64sha256}"
  runtime          = "nodejs8.10"
  publish          = true
  vpc_config {
    security_group_ids = ["sg-11111"]
    subnet_ids         = ["subnet-111111", "subnet-222222"]
  }

}

resource "aws_lambda_alias" "prod" {
  name             = "prod_${aws_lambda_function.put_endpoint.version}"
  function_name    = "${aws_lambda_function.put_endpoint.arn}"
  function_version = "${aws_lambda_function.put_endpoint.version}"
  routing_config   = {
    additional_version_weights = "${map(
      "${aws_lambda_function.put_endpoint.version - 1}", "0.5",
    )}"

  }
}
