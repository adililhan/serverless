const pgPool = require('pg-pool');
const moment = require('moment');
const config = require('./config.json');
const pool = new pgPool(config);

exports.handler = async(event) => {

    var name = event.pathParameters.name;
    
    return getUser(name).then(result => {
    var start = moment({}, "YYYY-MM-DD");
    var end = moment(result.birthday, "YYYY-MM-DD");
    var check = moment.duration(start.diff(end)).asDays();

        const response = {
          statusCode: 200,
          body: JSON.stringify({message: `Hello, ${name}!, Your birthday is in ${check}`})
        };

        
        return response;
    }).catch(error => {
        console.error(error.toString());
        const response = {
          statusCode: 500,
          body: JSON.stringify("Something went wrong. Error no: 1003")
        };
        
        return response;
    })
    
};

function getUser(name) {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT birthday FROM users WHERE name = $1`,
                [name]
            )
            .then(result => {
                resolve(result.rows[0]);
            }).catch(error => {
                reject(error);
            });
        }
    );
}