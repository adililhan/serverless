provider "aws" {
  region     = "eu-central-1"
  access_key = ""
  secret_key = ""
}

data "archive_file" "get_lambda_zip" {
    type          = "zip"
    source_dir    = "./get"
    output_path   = "get_endpoint.zip"
}

resource "aws_lambda_function" "get_endpoint" {
  filename         = "get_endpoint.zip"
  function_name    = "get_endpoint"
  role             = "arn:aws:iam::1111111:role/service-role/myrole1"
  handler          = "index.handler"
  source_code_hash = "${data.archive_file.get_lambda_zip.output_base64sha256}"
  runtime          = "nodejs8.10"
  publish          = true
  vpc_config {
    security_group_ids = ["sg-11111"]
    subnet_ids         = ["subnet-111111", "subnet-222222"]
  }

}

resource "aws_lambda_alias" "prod" {
  name             = "prod_${aws_lambda_function.get_endpoint.version}"
  function_name    = "${aws_lambda_function.get_endpoint.arn}"
  function_version = "${aws_lambda_function.get_endpoint.version}"
  routing_config   = {
    additional_version_weights = "${map(
      "${aws_lambda_function.get_endpoint.version - 1}", "0.5",
    )}"

  }
}
